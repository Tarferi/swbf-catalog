package org.spigotl.catalog;

import java.io.File;
import java.nio.file.Files;

import net.sf.sevenzipjbinding.SevenZipException;

import org.gjt.sp.util.ProgressObserver;
import org.spigotl.catalog.utils.ExternalExtract;

public class CatalogMap {

	private String code;
	private String name;
	private String url;
	private long size;
	private String ssize;
	private String author;

	protected CatalogMap(String code, String name, long size, String strsize, String url, String author) {
		this.code = code;
		this.name = name;
		this.size = size;
		this.url = url;
		this.author = author;
		this.ssize = strsize;
	}

	public String getURL() {
		return url;
	}

	public String getSizeString() {
		return ssize;
	}

	public long getSize() {
		return size;
	}

	public String getCode() {
		return code;
	}

	public String getName() {
		return name;
	}

	public String getAuthor() {
		return author;
	}

	public void get(final CatalogHandler h) {
		h.disableTable();
		try {
			MapsCatalog.getMap(this, new ProgressObserver() {

				@Override
				public void setMaximum(long m) {
				}

				@Override
				public void setStatus(String f) {
				}

				@Override
				public void setValue(long v) {
				}

			}, h.getOperationListener(), new CatalogOperationHandler() {

				@Override
				public void operationFailed() {
					h.enableTable();
					h.setProgressText("Download failed!");
				}

				@Override
				public void operationFinished(File f, String name) {
					File tf = null;
					try {
						String targetdir = "maps/";
						String targetfile = targetdir + "/" + name;
						tf = new File(targetfile);
						new File(targetdir).mkdirs();
						Files.copy(f.toPath(), tf.toPath());
						Extractor e = new ExternalExtract("addme.script");
						/*
						 * if (name.toLowerCase().endsWith(".zip")) { e = new ZipUtils().setWatchFile("addme.script"); } else if (name.toLowerCase().endsWith(".rar")) { e = new RarUtils().setWatchFile("addme.script"); }
						 */
						try {
							if (!e.extract(targetfile, targetdir)) {
								h.enableTable();
								h.setProgressText("Error: File extract failed.");
								tf.delete();
								SafeLogger.logBadMap(CatalogMap.this);
								return;
							}
						} catch (SevenZipException ep) {
							if (ep.getMessage().equals("Archive file can't be opened with none of the registered codecs")) {
								System.err.println("ERROR: Failed to extract file: no valid archive");
								SafeLogger.logBadMap(CatalogMap.this, "BADARCHIVE");
								return;
							}
						}
						File ff = e.getFile();
						if (ff != null) {
							handle(ff);
							h.enableTable();
							h.setProgressText("OK");
						} else {
							SafeLogger.logBadMap(CatalogMap.this, "NOADDME");
							h.enableTable();
							h.setProgressText("FAIL: No addme script found");
						}
					} catch (Throwable e) {
						h.enableTable();
						e.printStackTrace();
						h.setProgressText("Map install failed (" + e.getMessage() + ")!");
						if (tf != null) {
							tf.delete();
						}
					}
				}

			});
		} catch (Exception e) {
			h.enableTable();
			h.setProgressText("Map install failed (" + e.getMessage() + ")!");
		}
	}

	private void handle(File ff) {
		Map[] maps = addmeScriptParser.getMap(ff);
		if (maps == null) {
			System.err.println("ERROR: map list went out empty.");
		} else {
			for (Map m : maps) {
				SafeLogger.logMap(m, this);
			}
		}
	}

	public interface CatalogOperationHandler {

		public void operationFailed();

		public void operationFinished(File f, String name);

	}
}
