package org.spigotl.catalog.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.gjt.sp.util.IOUtilities;
import org.gjt.sp.util.ProgressObserver;

public class URLRetriever {
	private boolean isClosed = false;
	static final String COOKIES_HEADER = "Set-Cookie";
	private String url;
	private static final String AGENT = "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36";
	private static final String ACCEPT = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8";
	private static final String ACCEPTENC = "gzip, deflate, sdch";
	private static final String ACCEPTLANG = "cs,en;q=0.8,sk;q=0.6";

	private String response;
	private CloseableHttpClient client;
	private boolean post = false;
	private String lpt;
	List<NameValuePair> params = new ArrayList<NameValuePair>(2);

	public void addPostField(String key, String value) {
		params.add(new BasicNameValuePair(key, value));
	}

	public void setPost() {
		post = true;
	}

	public void setGet() {
		post = false;
	}

	public void setURL(String url) {
		this.url = url;
	}

	public String getLastPageTitle() {
		return lpt;
	}

	private void updateData() {
		if (response != null) {
			int p1 = response.indexOf("<title>") + "<title>".length();
			int p2 = response.indexOf("</title>", p1);
			try {
				lpt = response.substring(p1, p2).trim();
			} catch (Exception e) {
				lpt = null;
			}
		} else {
			lpt = null;
		}
	}

	private List<String> getData = new ArrayList<>();

	@SuppressWarnings("deprecation")
	public void addGETData(String key, String data) {
		getData.add(key + "=" + URLEncoder.encode(data));
	}

	public void commitCurrent() {
		if (isClosed) {
			System.err.println("Failed to execute retrieve operation on closed client!");
			return;
		}
		try {
			if (getData.size() > 0) {
				String u = TextUtils.join("&", getData);
				if (!url.contains("?")) {
					url = url + "?" + u;
				} else {
					url = url + "&" + u;
				}
				getData.clear();
			}
			HttpUriRequest request;
			if (post) {
				request = new HttpPost(url);
				((HttpPost) request).setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
			} else {
				request = new HttpGet(url);
			}
			params.clear();
			request.addHeader("User-Agent", AGENT);
			request.addHeader("Accept", ACCEPT);
			request.addHeader("Accept-Encoding", ACCEPTENC);
			request.addHeader("Accept-Language", ACCEPTLANG);
			CloseableHttpResponse resp = client.execute(request);
			StringWriter writer = new StringWriter();
			IOUtils.copy(resp.getEntity().getContent(), writer, Charset.defaultCharset());
			response = writer.toString();
			resp.close();
			updateData();
		} catch (Exception e) {
			e.printStackTrace();
			response = null;
		}
	}

	public InputStream commitCurrentGetInput() {
		if (isClosed) {
			System.err.println("Failed to execute retrieve operation on closed client!");
			return null;
		}
		try {
			if (getData.size() > 0) {
				String u = TextUtils.join("&", getData);
				if (!url.contains("?")) {
					url = url + "?" + u;
				} else {
					url = url + "&" + u;
				}
				getData.clear();
			}
			HttpUriRequest request;
			if (post) {
				request = new HttpPost(url);
				((HttpPost) request).setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
			} else {
				request = new HttpGet(url);
			}
			params.clear();
			request.addHeader("User-Agent", AGENT);
			request.addHeader("Accept", ACCEPT);
			request.addHeader("Accept-Encoding", ACCEPTENC);
			request.addHeader("Accept-Language", ACCEPTLANG);
			HttpResponse resp = client.execute(request);
			return resp.getEntity().getContent();
		} catch (Exception e) {
			e.printStackTrace();
			response = null;
			return null;
		}
	}

	public boolean hasResponse() {
		return response != null;
	}

	public String getResponse() {
		return response;
	}

	public URLRetriever() {
		client = HttpClientBuilder.create().build();
	}

	private static String serialize(HashMap<String, String> data) {
		StringBuilder sb = new StringBuilder();
		Object[] keys = data.keySet().toArray();
		Object[] values = data.values().toArray();
		int len = keys.length;
		for (int i = 0; i < len; i++) {
			sb.append(keys[i] + "=" + values[i] + "&");
		}
		if (sb.length() > 0) {
			sb.setLength(sb.length() - 1);
		}
		return sb.toString();
	}

	public static final File getURLContentToFile(String url, ProgressObserver p) {
		try {
			URL urll = new URL(url);
			HttpURLConnection conn = (HttpURLConnection) urll.openConnection();
			conn.setDoOutput(true);
			File tmp = File.createTempFile("tmpcache", "swbfmap");
			FileOutputStream out = new FileOutputStream(tmp);
			IOUtilities.copyStream(p, conn.getInputStream(), out, true);
			return tmp;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static final String getURLContent(String url, HashMap<String, String> postData, HashMap<String, String> headers) {
		try {
			URL urll = new URL(url);
			HttpURLConnection conn = (HttpURLConnection) urll.openConnection();
			conn.setDoOutput(true);
			Object[] hkey = headers.keySet().toArray();
			Object[] hvals = headers.values().toArray();
			int len = hkey.length;
			for (int i = 0; i < len; i++) {
				conn.addRequestProperty((String) hkey[i], (String) hvals[i]);
			}
			String data = serialize(postData);
			conn.getOutputStream().write(data.getBytes());
			StringWriter writer = new StringWriter();
			IOUtils.copy(conn.getInputStream(), writer, Charset.defaultCharset());
			String rdata = writer.toString();
			return rdata;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static final String getURLContent(String url) {
		try {
			URL urll = new URL(url);
			URLConnection conn = urll.openConnection();
			StringWriter writer = new StringWriter();
			IOUtils.copy(conn.getInputStream(), writer, Charset.defaultCharset());
			String rdata = writer.toString();
			return rdata;
		} catch (Exception e) {
			return null;
		}
	}

	public String getCurrentURL() {
		return url;
	}

	public void close() {
		try {
			isClosed = true;
			client.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
