package org.spigotl.catalog.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;

import net.sf.sevenzipjbinding.ISequentialOutStream;
import net.sf.sevenzipjbinding.ISevenZipInArchive;
import net.sf.sevenzipjbinding.SevenZip;
import net.sf.sevenzipjbinding.SevenZipException;
import net.sf.sevenzipjbinding.SevenZipNativeInitializationException;
import net.sf.sevenzipjbinding.impl.RandomAccessFileInStream;
import net.sf.sevenzipjbinding.simple.ISimpleInArchiveItem;

import org.spigotl.catalog.Extractor;

public class ExternalExtract extends Extractor {

	public ExternalExtract(String look) {
		super(look);
	}

	static {
		try {
			SevenZip.initSevenZipFromPlatformJAR();
		} catch (SevenZipNativeInitializationException e) {
			e.printStackTrace();
		}
	}

	@Override
	protected boolean extractTo(String file, String dir) throws SevenZipException {
		RandomAccessFile randomAccessFile = null;
		ISevenZipInArchive inArchive = null;
		try {
			randomAccessFile = new RandomAccessFile(file, "r");
			inArchive = SevenZip.openInArchive(null, new RandomAccessFileInStream(randomAccessFile));
			ISimpleInArchiveItem[] items = inArchive.getSimpleInterface().getArchiveItems();
			for (ISimpleInArchiveItem item : items) {
				File f = new File(dir + "/" + item.getPath());
				if (item.isFolder()) {
					f.mkdirs();
				} else {
					f.getParentFile().mkdirs();
					f.createNewFile();
					final FileOutputStream out = new FileOutputStream(f);
					item.extractSlow(new ISequentialOutStream() {

						@Override
						public int write(byte[] b) throws SevenZipException {
							try {
								out.write(b);
							} catch (IOException e) {
								e.printStackTrace();
							}
							return b.length;
						}

					});
					out.close();
					super.extractingFile(f);
				}
			}
		} catch (Exception e) {
			if (e instanceof SevenZipException) {
				throw (SevenZipException) e;
			}
			e.printStackTrace();
			return false;
		} finally {
			if (inArchive != null) {
				try {
					inArchive.close();
				} catch (SevenZipException e) {
				}
			}
			if (randomAccessFile != null) {
				try {
					randomAccessFile.close();
				} catch (IOException e) {
				}
			}
		}
		return true;
	}
}