package org.spigotl.catalog;

import java.util.List;

public interface CatalogOperationListener {

	public void operationInProgress(String operation);

	public void lockData();
	
	public void unlockData();
	
	public void setData(List<CatalogMap> data);
}
