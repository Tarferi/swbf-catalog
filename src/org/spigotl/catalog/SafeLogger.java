package org.spigotl.catalog;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

public class SafeLogger {
	private static final List<String> urls = new ArrayList<>();
	static {
		out = new File("code.txt");
		delim = new String(new byte[] { 0x1 });
		load();
	}

	public static boolean hasMap(CatalogMap m) {
		return urls.contains(m.getURL());
	}

	private static final String delim;

	private static final File out;

	public static void logMap(Map m, CatalogMap n) {
		urls.add(n.getURL());
		String code = m.getMapCode() + delim + m.getEra() + delim + m.getReinforcements1() + delim + m.getReinforcements2() + delim + m.getPlanet() + delim + m.getBattle() + delim + n.getAuthor() + delim + n.getName() + delim + n.getSize() + delim + n.getSizeString() + delim + n.getURL();
		write(code);
	}

	private static void load() {
		try {
			String data = new String(Files.readAllBytes(out.toPath()));
			String[] q = data.split("\r\n");
			for (String d : q) {
				String[] b = d.split(delim);
				if (b.length == 11) {
					String url = b[10];
					urls.add(url);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void write(String code) {
		FileOutputStream s = null;
		if (!out.exists()) {
			try {
				out.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		try {
			s = new FileOutputStream(out, true);
			s.write((code + "\r\n").getBytes());
			close(s);
		} catch (Exception e) {
			close(s);
			e.printStackTrace();
		}
	}

	private static void close(FileOutputStream s) {
		try {
			s.close();
		} catch (Throwable e) {
		}
	}

	private static String getInput(String echo) {
		try {
			System.out.write(echo.getBytes());
		} catch (IOException e1) {
		}
		InputStream in = System.in;
		StringBuilder sb = new StringBuilder();
		while (true) {
			try {
				int i = in.read();
				if (i == -1) {
					break;
				}
				char c = (char) i;
				if (c == '\r' || c == '\n') {
					break;
				}
				sb.append(c);
			} catch (IOException e) {
				return "INERROR";
			}
		}
		return sb.toString();
	}

	public static void logBadMap(CatalogMap catalogMap) {
		String reason = getInput("Reason: ");
		logBadMap(catalogMap, reason);
	}

	public static void logBadMap(CatalogMap catalogMap, String reason) {
		if(reason==null) {
			reason="INVALIDREASON";
		}
		if(reason.isEmpty()) {
			reason=getInput("Reason: ");
			if(reason==null) {
				reason="INVALIDREASON";
			}
			if(reason.isEmpty()) {
				reason="UNWILLINGREASON";
			}
		}
		String c = "TRASH";
		String code = reason + delim + c + delim + c + delim + c + delim + c + delim + c + delim + c + delim + c + delim + c + delim + c + delim + catalogMap.getURL();
		write(code);
	}

}
