package org.spigotl.catalog;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MAIN {

	public static void main(String[] args) {
		// boolean b = new ExternalExtract("addme.script").extract("C:\\Documents and Settings\\Tom\\workspace\\SWBFCatalog\\maps\\Phobos_Common_Jan2015.7z", "C:\\Documents and Settings\\Tom\\workspace\\SWBFCatalog\\maps\\");
		// System.out.println("B: "+b);
		load();
	}

	private static void load() {
		Logger.getLogger("org.apache.http.client.protocol.ResponseProcessCookies").setLevel(Level.OFF);
		MapsCatalog.getCatalogMaps(new CatalogOperationListener() {

			@Override
			public void operationInProgress(String operation) {
				System.out.println(operation);
			}

			@Override
			public void lockData() {
			}

			@Override
			public void unlockData() {
			}

			@Override
			public void setData(List<CatalogMap> data) {
				handle(data);
			}

		});
	}

	protected static void handle(List<CatalogMap> data) {
		CatalogHandler handler = new CatalogHandler() {

			@Override
			public void setProgressText(String string) {
				System.out.println(string);
			}

		};
		for (CatalogMap m : data) {
			if (!SafeLogger.hasMap(m)) {
				m.get(handler);
			} else {
				handler.setProgressText("Skipping " + m.getName());
			}
		}
	}

}
