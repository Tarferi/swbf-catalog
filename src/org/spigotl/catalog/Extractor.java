package org.spigotl.catalog;

import java.io.File;
import java.nio.file.Files;
import java.util.UUID;

import net.sf.sevenzipjbinding.SevenZipException;

import org.apache.commons.io.FileUtils;

public abstract class Extractor {

	private String name;
	private File found = null;

	public Extractor(String look) {
		this.name = look;
	}

	public boolean extract(String file, String dir) throws SevenZipException {
		try {
			String tmpdir = dir + "/safeToDelete/" + UUID.randomUUID().toString();
			File d = new File(tmpdir);
			d.mkdirs();
			File f8;
			if (!extractTo(file, tmpdir)) {
				System.out.println("Fail 1");
				f8 = new File(dir + "/safeToDelete/" + file);
				f8.getParentFile().mkdirs();
				if (f8.exists()) {
					f8.delete();
				}
				Files.copy(new File(file).toPath(), f8.toPath());
				FileUtils.deleteDirectory(d);
				return false;
			}
			if (found == null) {
				System.out.println("Fail 2");
				f8 = new File(dir + "/safeToDelete/" + file);
				f8.getParentFile().mkdirs();
				if (f8.exists()) {
					f8.delete();
				}
				Files.copy(new File(file).toPath(), f8.toPath());
				FileUtils.deleteDirectory(d);
				return false;
			}
			File from = found.getParentFile();
			File to = new File(dir + "/" + found.getParentFile().getName());
			found = new File(to.getAbsolutePath() + "/" + found.getName());
			if (!from.getAbsolutePath().equals(to.getAbsolutePath())) {
				FileUtils.copyDirectory(from, to);
				new File(file).delete();
				FileUtils.deleteDirectory(d);
			} else {
				System.err.println("Error: Failed to copy: Source and Destination are the same");
			}
			FileUtils.deleteDirectory(d);
			return true;
		} catch (Throwable e) {
			if (e instanceof SevenZipException) {
				throw (SevenZipException) e;
			}
			e.printStackTrace();
			System.out.println("Fail 3");
			return false;
		}
	}

	protected abstract boolean extractTo(String file, String dir) throws SevenZipException;

	protected void extractingFile(File target) {
		if (target.getName().toLowerCase().equals(name)) {
			this.found = target;
		}
	}

	public File getFile() {
		return found;
	}
}
