package org.spigotl.catalog;

import java.util.List;

public abstract class CatalogHandler {

	private CatalogOperationListener h;

	public void disableTable() {
	}

	public void setProgressValue(int i) {
	}

	public CatalogHandler() {
		this.h = new CatalogOperationListener() {

			@Override
			public void operationInProgress(String operation) {
				System.out.println(operation);
			}

			@Override
			public void lockData() {
			}

			@Override
			public void unlockData() {
			}

			@Override
			public void setData(List<CatalogMap> data) {
			}

		};
	}

	public CatalogOperationListener getOperationListener() {
		return h;
	}

	public abstract void setProgressText(String string);

	public void enableTable() {
	}

}
